﻿namespace MyNancy.Modules{

    using Models;

    using Nancy;

    public class HomeModule : NancyModule{

        public HomeModule(){
            Get["/"] = p =>{
                //return View["/Home"];
                return View["/Home", new Person(){Name = "saber"}];
            };
        }

    }

}